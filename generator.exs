alias CineasteInterfaceWeb.{AboutView, PageView, PeopleView, FilmView, LayoutView}
alias CineasteData.{ShowcasedFilms, ShowcasedPeople}

import Phoenix.View

assets_dir = "apps/cineaste_interface/priv/static"
layout_assigns = %{layout: {LayoutView, "app.html"}}

showcased_films = ShowcasedFilms.get_all()
showcased_people = ShowcasedPeople.get_all()

# clean up existing public folder
File.rm_rf("public")

# make static assets dir from phoenix app the new public folder
File.cp_r("#{assets_dir}", "public")

# add static page specific js
File.cp("static.js", "public/js/static.js")

# refresh page directories

File.mkdir("public/about")
File.mkdir("public/films")
File.mkdir("public/people")

# indexes
File.write("public/index.html", render_to_string(PageView, "index.html", layout_assigns))
File.write("public/about/index.html", render_to_string(AboutView, "index.html", layout_assigns))
File.write("public/films/index.html", render_to_string(FilmView, "index.html", Map.put(layout_assigns, :views, showcased_films)))
File.write("public/people/index.html", render_to_string(PeopleView, "index.html", Map.put(layout_assigns, :views, showcased_people)))

# films
Enum.each(showcased_films, fn f ->
  File.write("public/films/#{f[:path]}.html", render_to_string(FilmView, "show.html", Map.put(layout_assigns, :view, f)))
end)

# people
Enum.each(showcased_people, fn p ->
  File.write("public/people/#{p[:path]}.html", render_to_string(PeopleView, "show.html", Map.put(layout_assigns, :view, p)))
end)
