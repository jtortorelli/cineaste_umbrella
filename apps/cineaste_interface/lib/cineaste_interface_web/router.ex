defmodule CineasteInterfaceWeb.Router do
  use CineasteInterfaceWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CineasteInterfaceWeb do
    pipe_through :browser # Use the default browser stack

#    get "/*path", PageController, :index
    get("/", PageController, :index)
    get("/films", FilmController, :index)
    get("/films/:id", FilmController, :show)
    get("/people", PeopleController, :index)
    get("/people/:id", PeopleController, :show)
    get("/about", AboutController, :index)
  end

  # Other scopes may use custom stacks.
  # scope "/api", CineasteInterfaceWeb do
  #   pipe_through :api
  # end
end
