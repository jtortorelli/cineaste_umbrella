use Mix.Config

config :cineaste_data,
  credits_base_path: "apps/cineaste_data/lib/cineaste_data/credits",
  synopses_base_path: "apps/cineaste_data/lib/cineaste_data/synopses",
  bios_base_path: "apps/cineaste_data/lib/cineaste_data/bios"
