defmodule CineasteInterfaceWeb.AboutController do
  use CineasteInterfaceWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end