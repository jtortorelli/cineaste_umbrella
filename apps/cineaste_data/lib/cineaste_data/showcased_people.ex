defmodule CineasteData.ShowcasedPeople do
  use GenServer
  import DataUtil

  #######
  # API #
  #######

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def get_all() do
    GenServer.call(__MODULE__, :all)
  end

  def get_showcased(person_id) do
    GenServer.call(__MODULE__, {:get_showcased, person_id})
  end

  def get_by_path(path) do
    GenServer.call(__MODULE__, {:get_by_path, path})
  end

  #############
  # Callbacks #
  #############

  def init(_) do
    {:ok, %{all: get(), map: get_map(), path_map: get_path_map()}}
  end

  def handle_call(:all, _from, %{all: all} = state) do
    {:reply, all, state}
  end

  def handle_call({:get_showcased, person_id}, _from, %{map: map} = state) do
    with proper_id <- derive_person_id(person_id) do
      if Map.has_key?(map, proper_id) do
        {:reply, Map.get(map, proper_id), state}
      else
        {:reply, nil, state}
      end
    end
  end

  def handle_call({:get_by_path, path}, _from, %{path_map: path_map} = state) do
    if Map.has_key?(path_map, path) do
      {:reply, Map.get(path_map, path), state}
    else
      {:reply, nil, state}
    end
  end

  def get do
    [
      CineasteData.Person.AdamsNick.get(),
      CineasteData.Person.AiKyoko.get(),
      CineasteData.Person.AmamotoHideyo.get(),
      CineasteData.Person.AnzaiKyoko.get(),
      CineasteData.Person.ArikawaSadamasa.get(),
      CineasteData.Person.ArishimaIchiro.get(),
      CineasteData.Person.AsahiyoTaro.get(),
      CineasteData.Person.ChiakiMinoru.get(),
      CineasteData.Person.ConwayHaroldS.get(),
      CineasteData.Person.DanIkuma.get(),
      CineasteData.Person.DunhamRobert.get(),
      CineasteData.Person.EchigoKen.get(),
      CineasteData.Person.EnomotoKenichi.get(),
      CineasteData.Person.FujikiYu.get(),
      CineasteData.Person.FujimotoSanezumi.get(),
      CineasteData.Person.FujitaSusumu.get(),
      CineasteData.Person.FujiwaraKamatari.get(),
      CineasteData.Person.FujiyamaYoko.get(),
      CineasteData.Person.FukudaJun.get(),
      CineasteData.Person.FunatoJun.get(),
      CineasteData.Person.FurnessGeorgeA.get(),
      CineasteData.Person.FuruhataKoji.get(),
      CineasteData.Person.GondoYukihiko.get(),
      CineasteData.Person.HamaMie.get(),
      CineasteData.Person.HaraSetsuko.get(),
      CineasteData.Person.HasegawaHiroshi.get(),
      CineasteData.Person.HidariBokuzen.get(),
      CineasteData.Person.HiguchiShinji.get(),
      CineasteData.Person.HinataKazuo.get(),
      CineasteData.Person.HirataAkihiko.get(),
      CineasteData.Person.HiroseShoichi.get(),
      CineasteData.Person.HodgsonWilliamHope.get(),
      CineasteData.Person.HondaIshiro.get(),
      CineasteData.Person.HoshiYuriko.get(),
      CineasteData.Person.HughesAndrew.get(),
      CineasteData.Person.IbukiToru.get(),
      CineasteData.Person.IfukubeAkira.get(),
      CineasteData.Person.IkebeRyo.get(),
      CineasteData.Person.IketaniSaburo.get(),
      CineasteData.Person.ImaizumiRen.get(),
      CineasteData.Person.InagakiHiroshi.get(),
      CineasteData.Person.InoueYasuyuki.get(),
      CineasteData.Person.IshidaShigeki.get(),
      CineasteData.Person.ItoHisaya.get(),
      CineasteData.Person.ItoJerry.get(),
      CineasteData.Person.ItoMinoru.get(),
      CineasteData.Person.KagawaKyoko.get(),
      CineasteData.Person.KamiyaMakoto.get(),
      CineasteData.Person.KatoDaisuke.get(),
      CineasteData.Person.KatoHaruya.get(),
      CineasteData.Person.KatoShigeo.get(),
      CineasteData.Person.KatsubeYoshio.get(),
      CineasteData.Person.KawaiKenji.get(),
      CineasteData.Person.KawazuSeizaburo.get(),
      CineasteData.Person.KayamaShigeru.get(),
      CineasteData.Person.KirinoNadao.get(),
      CineasteData.Person.KitaTakeo.get(),
      CineasteData.Person.KobayashiKeiju.get(),
      CineasteData.Person.KobayashiTetsuko.get(),
      CineasteData.Person.KobayashiYukiko.get(),
      CineasteData.Person.KochiMomoko.get(),
      CineasteData.Person.KodoKuninori.get(),
      CineasteData.Person.KoizumiHajime.get(),
      CineasteData.Person.KoizumiHiroshi.get(),
      CineasteData.Person.KondoKeiko.get(),
      CineasteData.Person.KosekiYuji.get(),
      CineasteData.Person.KosugiYoshio.get(),
      CineasteData.Person.KuboAkira.get(),
      CineasteData.Person.KumagaiTakuzo.get(),
      CineasteData.Person.KunoSeishiro.get(),
      CineasteData.Person.KurobeSusumu.get(),
      CineasteData.Person.KurosawaAkira.get(),
      CineasteData.Person.KurosawaToshio.get(),
      CineasteData.Person.KusabueMitsuko.get(),
      CineasteData.Person.KusakawaNaoya.get(),
      CineasteData.Person.KusamaAkio.get(),
      CineasteData.Person.KimuraTakeshi.get(),
      CineasteData.Person.MachanLittleMan.get(),
      CineasteData.Person.MaedaBeverly.get(),
      CineasteData.Person.MaruyamaKenichiro.get(),
      CineasteData.Person.MatsubayashiShue.get(),
      CineasteData.Person.MatsumotoSomesho.get(),
      CineasteData.Person.MatsumuraTatsuo.get(),
      CineasteData.Person.MatsuoFuminto.get(),
      CineasteData.Person.MifuneToshiro.get(),
      CineasteData.Person.MihashiTatsuya.get(),
      CineasteData.Person.MikiNorihei.get(),
      CineasteData.Person.MillerLinda.get(),
      CineasteData.Person.MishimaKo.get(),
      CineasteData.Person.MiyauchiKunio.get(),
      CineasteData.Person.MiyazakiHayao.get(),
      CineasteData.Person.MizunoKumi.get(),
      CineasteData.Person.MukaiJunichiro.get(),
      CineasteData.Person.MurakamiFuyuki.get(),
      CineasteData.Person.MutsumiGoro.get(),
      CineasteData.Person.NakajimaHaruo.get(),
      CineasteData.Person.NakakitaChieko.get(),
      CineasteData.Person.NakamaruTadao.get(),
      CineasteData.Person.NakamuraGanjiro.get(),
      CineasteData.Person.NakamuraNobuo.get(),
      CineasteData.Person.NakamuraTetsu.get(),
      CineasteData.Person.NakanoTeruyoshi.get(),
      CineasteData.Person.NakayamaYutaka.get(),
      CineasteData.Person.NatsukiJunpei.get(),
      CineasteData.Person.NatsukiYosuke.get(),
      CineasteData.Person.NiheiMasanari.get(),
      CineasteData.Person.NomuraKozo.get(),
      CineasteData.Person.OdaMotoyoshi.get(),
      CineasteData.Person.OgataRinsaku.get(),
      CineasteData.Person.OkaYutaka.get(),
      CineasteData.Person.OkabeTadashi.get(),
      CineasteData.Person.OkamiJojiro.get(),
      CineasteData.Person.OkawaHenry.get(),
      CineasteData.Person.OkiShoji.get(),
      CineasteData.Person.OmaeWataru.get(),
      CineasteData.Person.OmuraSenkichi.get(),
      CineasteData.Person.OndaSeijiro.get(),
      CineasteData.Person.OnoeKatsuhiro.get(),
      CineasteData.Person.OshikawaShunro.get(),
      CineasteData.Person.OtomoShin.get(),
      CineasteData.Person.OtowaNobuko.get(),
      CineasteData.Person.OzawaEitaro.get(),
      CineasteData.Group.Peanuts.get(),
      CineasteData.Person.RankinArthur.get(),
      CineasteData.Person.ReasonRhodes.get(),
      CineasteData.Person.RyuChishu.get(),
      CineasteData.Person.SadaYutaka.get(),
      CineasteData.Person.SaharaKenji.get(),
      CineasteData.Person.SaijoYasuhiko.get(),
      CineasteData.Person.SakaiFrankie.get(),
      CineasteData.Person.SakaiSachio.get(),
      CineasteData.Person.SakakidaKeiji.get(),
      CineasteData.Person.SakamotoHaruya.get(),
      CineasteData.Person.SanjoMiki.get(),
      CineasteData.Person.SasakiTakamaru.get(),
      CineasteData.Person.SataKeiko.get(),
      CineasteData.Person.SatoMakoto.get(),
      CineasteData.Person.SatoMasaru.get(),
      CineasteData.Person.SatoNaoki.get(),
      CineasteData.Person.SawaiKeiko.get(),
      CineasteData.Person.SawamuraIkio.get(),
      CineasteData.Person.SawamuraSonosuke.get(),
      CineasteData.Person.SekitaHiroshi.get(),
      CineasteData.Person.SekizawaShinichi.get(),
      CineasteData.Person.SendaKoreya.get(),
      CineasteData.Person.SeraAkira.get(),
      CineasteData.Person.ShibuyaHideo.get(),
      CineasteData.Person.ShimizuGen.get(),
      CineasteData.Person.ShimozawaKan.get(),
      CineasteData.Person.ShimuraTakashi.get(),
      CineasteData.Person.ShinoharaMasaki.get(),
      CineasteData.Person.ShiozawaToki.get(),
      CineasteData.Person.ShirakawaYumi.get(),
      CineasteData.Person.SonodaAyumi.get(),
      CineasteData.Person.SugimuraHaruko.get(),
      CineasteData.Person.SunazukaHideo.get(),
      CineasteData.Person.SuzukiHaruo.get(),
      CineasteData.Person.SuzukiKazuo.get(),
      CineasteData.Person.TabuKenzo.get(),
      CineasteData.Person.TachibanaMasaaki.get(),
      CineasteData.Person.TachikawaHiroshi.get(),
      CineasteData.Person.TajimaYoshibumi.get(),
      CineasteData.Person.TakadaMinoru.get(),
      CineasteData.Person.TakaradaAkira.get(),
      CineasteData.Person.TakashimaTadao.get(),
      CineasteData.Person.TamblynRuss.get(),
      CineasteData.Person.TanakaKinuyo.get(),
      CineasteData.Person.TanakaTomoyuki.get(),
      CineasteData.Person.TaniAkira.get(),
      CineasteData.Person.TaniguchiSenkichi.get(),
      CineasteData.Person.TazakiJun.get(),
      CineasteData.Person.TezukaKatsumi.get(),
      CineasteData.Person.ToginChotaro.get(),
      CineasteData.Person.TomitaNakajiro.get(),
      CineasteData.Person.TonoEijiro.get(),
      CineasteData.Person.TsubonoKamayuki.get(),
      CineasteData.Person.TsuburayaEiji.get(),
      CineasteData.Person.TsuchiyaShiro.get(),
      CineasteData.Person.TsuchiyaYoshio.get(),
      CineasteData.Person.TsudaMitsuo.get(),
      CineasteData.Person.TsukasaYoko.get(),
      CineasteData.Person.TsurutaKoji.get(),
      CineasteData.Person.TsutsumiYasuhisa.get(),
      CineasteData.Person.UbukataSoji.get(),
      CineasteData.Person.UedaKichijiro.get(),
      CineasteData.Person.UeharaKen.get(),
      CineasteData.Person.UeharaMisa.get(),
      CineasteData.Person.UemuraKenjiro.get(),
      CineasteData.Person.UnoKoji.get(),
      CineasteData.Person.WakabayashiAkiko.get(),
      CineasteData.Person.WakamatsuAkira.get(),
      CineasteData.Person.WakayamaSetsuko.get(),
      CineasteData.Person.WyattObel.get(),
      CineasteData.Person.YachigusaKaoru.get(),
      CineasteData.Person.YamadaKeisuke.get(),
      CineasteData.Person.YamadaMinosuke.get(),
      CineasteData.Person.YamamotoRen.get(),
      CineasteData.Person.YamamuraSo.get(),
      CineasteData.Person.YamazakiTakashi.get(),
      CineasteData.Person.YanagiyaKingoro.get(),
      CineasteData.Person.YashiroMiki.get(),
      CineasteData.Person.YusefOsman.get()
    ]
    |> Enum.map(fn x ->
      Map.put(x, :top_roles, derive_top_roles(x))
    end)
  end

  def derive_top_roles(%{selected_filmography: selected_filmography}) do
    selected_filmography
    |> Enum.map(fn {key, value} -> {key, Enum.count(value)} end)
    |> Enum.sort_by(&elem(&1, 1), &>=/2)
    |> Enum.take(3)
    |> Enum.map(&elem(&1, 0))
  end

  def get_map() do
    get()
    |> Enum.reduce(%{}, fn x, acc ->
      case x do
        %{qualifier: q} -> Map.put(acc, {derive_display_name(x), q}, x)
        _ -> Map.put(acc, {derive_display_name(x), 1}, x)
      end
    end)
  end

  def get_path_map() do
    get()
    |> Enum.reduce(%{}, fn x, acc -> Map.put(acc, x[:path], x) end)
  end
end
