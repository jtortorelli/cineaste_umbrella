defmodule CineasteInterfaceWeb.LayoutView do
  use CineasteInterfaceWeb, :view
  alias CineasteInterfaceWeb.PageTitle

  def page_title(assigns) do
    PageTitle.page_title(assigns)
  end
end
