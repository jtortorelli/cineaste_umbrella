
let indices = ['/about', '/films', '/people'];

let indicesRe = new RegExp(indices.map(function(i) {return i + '$'}).join("|"));

$(document).on("click", "a", function(e) {
    e.preventDefault();
    let href = $(this).attr('href');
    // index pages
    if (indicesRe.test(href)) {
        location.href = href + '/index.html';
        // all pages of form /{path}
    } else if (/\/.+$/.test(href)) {
        location.href = href + '.html';
        // root path
    } else {
        location.href = href;
    }
});