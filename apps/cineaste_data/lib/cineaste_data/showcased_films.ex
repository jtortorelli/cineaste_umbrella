defmodule CineasteData.ShowcasedFilms do
  use GenServer

  #######
  # API #
  #######

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def get_all() do
    GenServer.call(__MODULE__, :all)
  end

  def get_showcased(film_id) do
    GenServer.call(__MODULE__, {:get_showcased, film_id})
  end

  def get_by_path(path) do
    GenServer.call(__MODULE__, {:get_by_path, path})
  end

  #############
  # Callbacks #
  #############

  def init(_) do
    {:ok, %{all: get(), map: get_map(), path_map: get_path_map()}}
  end

  def handle_call(:all, _from, %{all: all} = state) do
    {:reply, all, state}
  end

  def handle_call({:get_showcased, film_id}, _from, %{map: map} = state) do
    if Map.has_key?(map, film_id) do
      {:reply, Map.get(map, film_id), state}
    else
      {:reply, nil, state}
    end
  end

  def handle_call({:get_by_path, path}, _from, %{path_map: path_map} = state) do
    if Map.has_key?(path_map, path) do
      {:reply, Map.get(path_map, path), state}
    else
      {:reply, nil, state}
    end
  end

  ###################
  # Private Methods #
  ###################

  defp get() do
    [
      CineasteData.Films.ThirteenAssassins2010.get(),
      CineasteData.Films.TwentiethCenturyBoys2008.get(),
      CineasteData.Films.TwentiethCenturyBoysRedemption2009.get(),
      CineasteData.Films.TwentiethCenturyBoysTheLastHope2009.get(),
      CineasteData.Films.AdventuresOfTaklamakan1966.get(),
      CineasteData.Films.AdventuresOfZatoichi1964.get(),
      CineasteData.Films.Akira1988.get(),
      CineasteData.Films.Alive2003.get(),
      CineasteData.Films.AllRoundAppraiserQTheEyesOfMonaLisa2014.get(),
      CineasteData.Films.Always2005.get(),
      CineasteData.Films.Always22007.get(),
      CineasteData.Films.Always32012.get(),
      CineasteData.Films.AManCalledPirate2016.get(),
      CineasteData.Films.Aragami2003.get(),
      CineasteData.Films.AssassinationClassroom2015.get(),
      CineasteData.Films.AssassinationClassroomGraduation2016.get(),
      CineasteData.Films.AssaultGirls2009.get(),
      CineasteData.Films.Atragon1963.get(),
      CineasteData.Films.AttackOnTitan2015.get(),
      CineasteData.Films.AttackOnTitanEndOfTheWorld2015.get(),
      CineasteData.Films.Avalon2001.get(),
      CineasteData.Films.Azumi2003.get(),
      CineasteData.Films.Azumi2DeathOrLove2005.get(),
      CineasteData.Films.Ballad2009.get(),
      CineasteData.Films.BattleInOuterSpace1959.get(),
      CineasteData.Films.BattleRoyale2000.get(),
      CineasteData.Films.BattleRoyaleIIRequiem2003.get(),
      CineasteData.Films.BirthOfJapan1959.get(),
      CineasteData.Films.BulletTrain1975.get(),
      CineasteData.Films.Casshern2004.get(),
      CineasteData.Films.CastleInTheSky1986.get(),
      CineasteData.Films.Crossfire2000.get(),
      CineasteData.Films.DaigoroVsGoliath1972.get(),
      CineasteData.Films.Daimajin1966.get(),
      CineasteData.Films.DaimajinStrikesAgain1966.get(),
      CineasteData.Films.DaredevilInTheCastle1961.get(),
      CineasteData.Films.DeathNote2006.get(),
      CineasteData.Films.DeathNoteLightUpTheNewWorld2016.get(),
      CineasteData.Films.DeathNoteTheLastName2006.get(),
      CineasteData.Films.DestroyAllMonsters1968.get(),
      CineasteData.Films.DogoraTheSpaceMonster1964.get(),
      CineasteData.Films.Dororo2007.get(),
      CineasteData.Films.EarthquakeArchipelago1980.get(),
      CineasteData.Films.EkoEkoAzarakWizardOfDarkness1995.get(),
      CineasteData.Films.EkoEkoAzarakIIBirthOfTheWizard1996.get(),
      CineasteData.Films.EkoEkoAzarakIIIMisaTheDarkAngel1998.get(),
      CineasteData.Films.Espy1974.get(),
      CineasteData.Films.EternalZero2013.get(),
      CineasteData.Films.EvilOfDracula1974.get(),
      CineasteData.Films.Explosion1975.get(),
      CineasteData.Films.FightZatoichiFight1964.get(),
      CineasteData.Films.FrankensteinConquersTheWorld1965.get(),
      CineasteData.Films.Friends2011.get(),
      CineasteData.Films.GameraTheGuardianOfTheUniverse1995.get(),
      CineasteData.Films.Gamera2AdventOfLegion1996.get(),
      CineasteData.Films.Gamera3RevengeOfIris1999.get(),
      CineasteData.Films.GameraTheBrave2006.get(),
      CineasteData.Films.GameraTheGiantMonster1965.get(),
      CineasteData.Films.GameraTheSpaceMonster1980.get(),
      CineasteData.Films.GameraVsBarugon1966.get(),
      CineasteData.Films.GameraVsGuiron1969.get(),
      CineasteData.Films.GameraVsGyaos1967.get(),
      CineasteData.Films.GameraVsJiger1970.get(),
      CineasteData.Films.GameraVsViras1968.get(),
      CineasteData.Films.GameraVsZigra1971.get(),
      CineasteData.Films.Gantz2011.get(),
      CineasteData.Films.GantzPerfectAnswer2011.get(),
      CineasteData.Films.Gatchaman2013.get(),
      CineasteData.Films.GenghisKhanToTheEndsOfTheEarthAndSea2007.get(),
      CineasteData.Films.Genocide1968.get(),
      CineasteData.Films.GhidorahTheThreeHeadedMonster1964.get(),
      CineasteData.Films.GhostInTheShell1995.get(),
      CineasteData.Films.GISamurai1979.get(),
      CineasteData.Films.GloriousTeamBatista2008.get(),
      CineasteData.Films.GMK2001.get(),
      CineasteData.Films.GodsLeftHandDevilsRightHand2006.get(),
      CineasteData.Films.Godzilla20001999.get(),
      CineasteData.Films.GodzillaFinalWars2004.get(),
      CineasteData.Films.GodzillaKingOfTheMonsters1954.get(),
      CineasteData.Films.GodzillaRaidsAgain1955.get(),
      CineasteData.Films.GodzillasRevenge1969.get(),
      CineasteData.Films.GodzillaVSBiollante1989.get(),
      CineasteData.Films.GodzillaVSDestroyer1995.get(),
      CineasteData.Films.GodzillaVsGigan1972.get(),
      CineasteData.Films.GodzillaVsHedorah1971.get(),
      CineasteData.Films.GodzillaVSKingGhidorah1991.get(),
      CineasteData.Films.GodzillaVSMechagodzilla1993.get(),
      CineasteData.Films.GodzillaVsMegalon1973.get(),
      CineasteData.Films.GodzillaVSMothra1992.get(),
      CineasteData.Films.GodzillaVSSpaceGodzilla1994.get(),
      CineasteData.Films.GodzillaVsTheCosmicMonster1974.get(),
      CineasteData.Films.GodzillaVsTheSeaMonster1966.get(),
      CineasteData.Films.GodzillaXMechagodzilla2002.get(),
      CineasteData.Films.GodzillaXMegaguirus2000.get(),
      CineasteData.Films.GodzillaXMothraXMechagodzillaTokyoSOS2003.get(),
      CineasteData.Films.Goemon2009.get(),
      CineasteData.Films.GokeBodySnatcherFromHell1968.get(),
      CineasteData.Films.GoldenBat1966.get(),
      CineasteData.Films.Gorath1962.get(),
      CineasteData.Films.Gunhed1989.get(),
      CineasteData.Films.HaraKiriDeathOfASamurai2011.get(),
      CineasteData.Films.HiddenFortress1958.get(),
      CineasteData.Films.HiddenFortressTheLastPrincess2008.get(),
      CineasteData.Films.HighAndLow1963.get(),
      CineasteData.Films.HMan1958.get(),
      CineasteData.Films.HowlsMovingCastle2004.get(),
      CineasteData.Films.HumanVapor1960.get(),
      CineasteData.Films.IAmAHero2016.get(),
      CineasteData.Films.InvisibleMan1954.get(),
      CineasteData.Films.JinRohTheWolfBrigade2000.get(),
      CineasteData.Films.Juvenile2000.get(),
      CineasteData.Films.K202008.get(),
      CineasteData.Films.Kagemusha1980.get(),
      CineasteData.Films.Kaiji2009.get(),
      CineasteData.Films.Kaiji22011.get(),
      CineasteData.Films.KamuiGaiden2009.get(),
      CineasteData.Films.KikisDeliveryService1989.get(),
      CineasteData.Films.KingKongEscapes1967.get(),
      CineasteData.Films.KingKongVsGodzilla1962.get(),
      CineasteData.Films.LChangeTheWorld2008.get(),
      CineasteData.Films.LakeOfDracula1971.get(),
      CineasteData.Films.LastWar1961.get(),
      CineasteData.Films.LatitudeZero1969.get(),
      CineasteData.Films.LegendOfTheEightSamurai1983.get(),
      CineasteData.Films.LibraryWars2013.get(),
      CineasteData.Films.LibraryWarsTheLastMission2015.get(),
      CineasteData.Films.LivingSkeleton1968.get(),
      CineasteData.Films.Lorelei2005.get(),
      CineasteData.Films.Lovedeath2007.get(),
      CineasteData.Films.LupinTheThird2014.get(),
      CineasteData.Films.LupinTheThirdTheCastleOfCagliostro1979.get(),
      CineasteData.Films.MagicSerpent1966.get(),
      CineasteData.Films.MakaiTenshoSamuraiReincarnation1981.get(),
      CineasteData.Films.MaskedRiderTheFirst2005.get(),
      CineasteData.Films.MaskedRiderTheNext2007.get(),
      CineasteData.Films.Matango1963.get(),
      CineasteData.Films.MechanicalViolatorHakaider1995.get(),
      CineasteData.Films.MessageFromSpace1978.get(),
      CineasteData.Films.MonsterZero1965.get(),
      CineasteData.Films.MoonOverTao1997.get(),
      CineasteData.Films.Mothra1961.get(),
      CineasteData.Films.MothraVsGodzilla1964.get(),
      CineasteData.Films.MushiShi2007.get(),
      CineasteData.Films.MyNeighborTotoro1988.get(),
      CineasteData.Films.Mysterians1957.get(),
      CineasteData.Films.NausicaaOfTheValleyOfTheWind1984.get(),
      CineasteData.Films.NewTaleOfZatoichi1963.get(),
      CineasteData.Films.OblivionIsland2009.get(),
      CineasteData.Films.OneMissedCall2003.get(),
      CineasteData.Films.OneMissedCall22005.get(),
      CineasteData.Films.OneMissedCallFinal2006.get(),
      CineasteData.Films.Onmyoji2001.get(),
      CineasteData.Films.OnmyojiII2003.get(),
      CineasteData.Films.ParasiteEve1997.get(),
      CineasteData.Films.Parasyte2014.get(),
      CineasteData.Films.ParasyteCompletion2015.get(),
      CineasteData.Films.PatlaborTheMovie1989.get(),
      CineasteData.Films.Patlabor2TheMovie1993.get(),
      CineasteData.Films.PlatinaData2013.get(),
      CineasteData.Films.Ponyo2008.get(),
      CineasteData.Films.PorcoRosso1992.get(),
      CineasteData.Films.PrincessBlade2001.get(),
      CineasteData.Films.PrincessMononoke1997.get(),
      CineasteData.Films.PropheciesOfNostradamus1974.get(),
      CineasteData.Films.Rasen1998.get(),
      CineasteData.Films.Rashomon1950.get(),
      CineasteData.Films.RebirthOfMothra1996.get(),
      CineasteData.Films.RebirthOfMothra21997.get(),
      CineasteData.Films.RebirthOfMothra31998.get(),
      CineasteData.Films.RedSpectacles1987.get(),
      CineasteData.Films.RescueWings2008.get(),
      CineasteData.Films.Returner2002.get(),
      CineasteData.Films.ReturnOfDaimajin1966.get(),
      CineasteData.Films.ReturnOfGodzilla1984.get(),
      CineasteData.Films.Ring1998.get(),
      CineasteData.Films.Ring0Birthday2000.get(),
      CineasteData.Films.Ring21999.get(),
      CineasteData.Films.Rodan1956.get(),
      CineasteData.Films.RurouniKenshin2012.get(),
      CineasteData.Films.RurouniKenshinKyotoInferno2014.get(),
      CineasteData.Films.RurouniKenshinTheLegendEnds2014.get(),
      CineasteData.Films.SakuyaSlayerOfDemons2000.get(),
      CineasteData.Films.SamaritanZatoichi1968.get(),
      CineasteData.Films.SamuraiCommandoMission15492005.get(),
      CineasteData.Films.SamuraiIMusashiMiyamoto1954.get(),
      CineasteData.Films.SamuraiIIDuelAtIchijojiTemple1955.get(),
      CineasteData.Films.SamuraiIIIDuelAtGanryuIsland1956.get(),
      CineasteData.Films.SamuraiPirate1963.get(),
      CineasteData.Films.Sanjuro1962.get(),
      CineasteData.Films.SayonaraJupiter1984.get(),
      CineasteData.Films.SecretOfTheTelegian1960.get(),
      CineasteData.Films.SevenSamurai1954.get(),
      CineasteData.Films.ShinGodzilla2016.get(),
      CineasteData.Films.ShinobiHeartUnderBlade2005.get(),
      CineasteData.Films.SinkingOfJapan2006.get(),
      CineasteData.Films.SkyCrawlers2008.get(),
      CineasteData.Films.SkyHigh2003.get(),
      CineasteData.Films.SonOfGodzilla1967.get(),
      CineasteData.Films.SpaceAmoeba1970.get(),
      CineasteData.Films.SpaceBattleshipYamato2010.get(),
      CineasteData.Films.SpiritedAway2001.get(),
      CineasteData.Films.StandByMeDoraemon2014.get(),
      CineasteData.Films.StrayDogKerberosPanzerCops1991.get(),
      CineasteData.Films.SubmersionOfJapan1973.get(),
      CineasteData.Films.SwordOfAlexander2007.get(),
      CineasteData.Films.TaleOfZatoichi1962.get(),
      CineasteData.Films.TaleOfZatoichiContinues1962.get(),
      CineasteData.Films.TalkingHead1992.get(),
      CineasteData.Films.TerrorOfMechagodzilla1975.get(),
      CineasteData.Films.Tetsujin282005.get(),
      CineasteData.Films.ThreeOutlawSamurai1964.get(),
      CineasteData.Films.ThroneOfBlood1957.get(),
      CineasteData.Films.TokyoBlackout1987.get(),
      CineasteData.Films.TopSecret2016.get(),
      CineasteData.Films.TriumphantGeneralRouge2009.get(),
      CineasteData.Films.TsubakiSanjuro2007.get(),
      CineasteData.Films.Ultraman2004.get(),
      CineasteData.Films.VampireDoll1970.get(),
      CineasteData.Films.VaranTheUnbelievable1958.get(),
      CineasteData.Films.Versus2000.get(),
      CineasteData.Films.WarInSpace1977.get(),
      CineasteData.Films.WarningFromSpace1956.get(),
      CineasteData.Films.WarOfTheGargantuas1966.get(),
      CineasteData.Films.Whirlwind1964.get(),
      CineasteData.Films.WindRises2013.get(),
      CineasteData.Films.XFromOuterSpace1967.get(),
      CineasteData.Films.YamatoTakeru1994.get(),
      CineasteData.Films.Yojimbo1961.get(),
      CineasteData.Films.Zatoichi1989.get(),
      CineasteData.Films.ZatoichiAndTheChessExpert1965.get(),
      CineasteData.Films.ZatoichiAndTheChestOfGold1964.get(),
      CineasteData.Films.ZatoichiAndTheDoomedMan1965.get(),
      CineasteData.Films.ZatoichiAndTheFugitives1968.get(),
      CineasteData.Films.ZatoichiAtLarge1972.get(),
      CineasteData.Films.ZatoichiChallenged1967.get(),
      CineasteData.Films.ZatoichiGoesToTheFireFestival1970.get(),
      CineasteData.Films.ZatoichiInDesperation1972.get(),
      CineasteData.Films.ZatoichiMeetsTheOneArmedSwordsman1971.get(),
      CineasteData.Films.ZatoichiMeetsYojimbo1970.get(),
      CineasteData.Films.ZatoichiOnTheRoad1963.get(),
      CineasteData.Films.ZatoichisCaneSword1967.get(),
      CineasteData.Films.ZatoichisConspiracy1973.get(),
      CineasteData.Films.ZatoichisFlashingSword1964.get(),
      CineasteData.Films.ZatoichisPilgrimage1966.get(),
      CineasteData.Films.ZatoichisRevenge1965.get(),
      CineasteData.Films.ZatoichisVengeance1966.get(),
      CineasteData.Films.ZatoichiTheFugitive1963.get(),
      CineasteData.Films.ZatoichiTheOutlaw1967.get(),
      CineasteData.Films.Zeiram1991.get(),
      CineasteData.Films.Zeiram21994.get()
    ]
  end

  defp get_map() do
    get()
    |> Enum.reduce(%{}, fn x, acc -> Map.put(acc, {x[:title], x[:release_date].year}, x) end)
  end

  defp get_path_map() do
    get()
    |> Enum.reduce(%{}, fn x, acc -> Map.put(acc, x[:path], x) end)
  end

end
